using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gnomik.AudioTools
{
    [System.Serializable]
    public class AudioEntry
    {
        public string name;
        public AudioClip[] clips;
        public float basePitch = 1f;
        public float minPitch = 1f;
        public float maxPitch = 1f;
    }

    [CreateAssetMenu(menuName = "AudioTools/Audio Library")]
    public class AudioLibrary : ScriptableObject
    {
        public AudioEntry[] entries;

        public void Play(AudioSource source, string name = null)
        {
            if (entries == null || entries.Length == 0)
            {
                Debug.LogWarning("No entries in library");
                return;
            }

            AudioEntry entry = entries[0];

            if (name != null)
            {
                foreach (AudioEntry e in entries)
                {
                    if (e.name == name)
                    {
                        entry = e;
                        break;
                    }
                }
            }

            if (entry.minPitch != entry.maxPitch)
            {
                source.pitch = Random.Range(entry.basePitch * entry.minPitch, entry.basePitch * entry.maxPitch);
            }

            source.PlayOneShot(entry.clips[Random.Range(0, entry.clips.Length)]);
        }
    }
}