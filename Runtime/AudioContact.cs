using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Gnomik.AudioTools
{

    [RequireComponent(typeof(AudioSource))]
    [RequireComponent(typeof(Collider))]
    public class AudioContact : MonoBehaviour
    {
        AudioSource source;

        public AudioLibrary library;

        float minPlayDelay = .5f;

        float lastPlayTime = 0;

        public bool checkSurface;

        private void Awake()
        {
            source = GetComponent<AudioSource>();
            gameObject.tag = "AudioContact";
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.CompareTag("AudioContact") && Time.time > lastPlayTime + minPlayDelay)
            {
                if (library != null)
                {
                    if (checkSurface && other.GetComponent<AudioSurface>() != null)
                    {
                        library.Play(source, other.GetComponent<AudioSurface>().surfaceName);
                    }
                    else
                    {
                        library.Play(source);
                    }

                }
                else
                {
                    source.Play();
                }
                lastPlayTime = Time.time;
            }

        }
    }
}