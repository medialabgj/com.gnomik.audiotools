using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Gnomik.AudioTools
{
    [CustomEditor(typeof(AudioContact))]
    public class AudioContactEditor : Editor
    {

        [MenuItem("GameObject/Audio/Audio Contact")]
        public static void CreateAudioEditor(MenuCommand menuCommand)
        {
            GameObject go = new GameObject("AudioContact");
            Collider audioCollider = go.AddComponent<SphereCollider>();
            audioCollider.isTrigger = true;
            AudioSource audioSource = go.AddComponent<AudioSource>();
            audioSource.playOnAwake = false;
            Rigidbody body = go.AddComponent<Rigidbody>();
            body.isKinematic = true;
            body.useGravity = false;
            AudioContact audioContact = go.AddComponent<AudioContact>();

            GameObjectUtility.SetParentAndAlign(go, menuCommand.context as GameObject);
            Undo.RegisterCreatedObjectUndo(go, "Create " + go.name);
            Selection.activeObject = go;
        }
    }

}